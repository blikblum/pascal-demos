program ClassHelpersAsm;

{$mode objfpc}{$H+}

uses
  fpjson;

function PlainFunction(Obj: TJSONObject; const Prop: String): String;
begin
  Result := Obj.Strings[Prop];
end;

type

  { TJSONObjectHelper }

  TJSONObjectHelper = class helper for TJSONObject
  public
    function HelperFunction(const Prop: String): String;
  end;

{ TJSONObjectHelper }

function TJSONObjectHelper.HelperFunction(const Prop: String): String;
begin
  Result := Self.Strings[Prop];
end;

var
  TheObject: TJSONObject;
  S: String;
begin
  TheObject := TJSONObject.Create;
  S := '';
  S := PlainFunction(TheObject, 'test');
  S := '';
  S := TheObject.HelperFunction('test');
  TheObject.Destroy;
end.

