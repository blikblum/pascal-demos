program ConstParameterShortStr;

{$Mode ObjFpc}
{$H+}
{.$IMPLICITEXCEPTIONS OFF}

uses
  SysUtils, Types;

procedure DoIt(const V: ShortString);
begin
  Writeln(V);
end;  
  
procedure ByValue(V: ShortString);
var
  S: ShortString;
begin
  S := V;  
  DoIt(S);
end;

procedure ByReference(const V: ShortString);
var
  S: ShortString;
begin
  S := V;
  DoIt(S);
end;

procedure ByValueReadOnly(V: ShortString);
begin
  DoIt(V);
end;

procedure ByReferenceReadOnly(const V: ShortString);
begin
  DoIt(V);
end;

var
  X: ShortString;

begin  
  X := 'Test';
  ByValue(X);
  ByReference(X);
end.
