program ConstParameterStr;

{$Mode ObjFpc}
{$H+}
{.$IMPLICITEXCEPTIONS OFF}

uses
  SysUtils, Types;

procedure DoIt(const V: String);
begin
  Writeln(V);
end;  
  
procedure ByValue(V: String);
begin
  V := V + 'x';
  DoIt(V);
end;

procedure ByReference(const V: String);
var
  S: String;
begin
  S := V + 'x';
  DoIt(S);
end;

procedure ByValueReadOnly(V: String);
begin
  DoIt(V);
end;

procedure ByReferenceReadOnly(const V: String);
begin
  DoIt(V);
end;

procedure ByReferenceOnlyFirst(const V: String; Z: String);
begin
  DoIt(V);
  DoIt(Z);
end;

procedure ByReferenceBoth(const V, Z: String);
begin
  DoIt(V);
  DoIt(Z);
end;

var
  X: String;

begin  
  X := 'Test';
  ByValueReadOnly(X);
  ByReferenceReadOnly(X);
  ByValue(X);
  ByReference(X);
end.
