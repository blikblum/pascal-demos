unit DialogResourceParser;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpjson, RegExpr;

type

  { TDialogResourceParser }

  TDialogResourceParser = class
  private
    FSource: TStrings;
    FControlExpr: TRegExpr;
    FGenericControlExpr: TRegExpr;
    procedure ParseDialogInfo(DialogData: TJSONObject);
    procedure ParseControls(DialogData: TJSONObject);
  public
    constructor Create(Source: TStrings);
    destructor Destroy; override;
    function Parse: TJSONObject;
  end;

implementation

const
  XScale = 1.5;
  YScale = 1.6;

function DialogUnitToPixelX(const Value: String): Integer;
begin
  Result := Trunc(StrToInt(Value) * XScale);
end;

function DialogUnitToPixelY(const Value: String): Integer;
begin
  Result := Trunc(StrToInt(Value) * YScale);
end;


{ TDialogResourceParser }

procedure TDialogResourceParser.ParseDialogInfo(DialogData: TJSONObject);
var
  Line: String;
  DialogRegEx: TRegExpr;
begin
  if FSource.Count = 0 then
    raise Exception.Create('Source is empty');
  DialogRegEx := TRegExpr.Create;
  DialogRegEx.Expression := '([\w]*)\s*DIALOGEX\s*(\d*),\s(\d*),\s(\d*),\s(\d*),\s(\d*)';
  Line := FSource[0];
  if not DialogRegEx.Exec(Line) then
    raise Exception.CreateFmt('Invalid resource header "%s"', [Line]);
  DialogData.Strings['name'] := DialogRegEx.Match[1];
  DialogData.Integers['left'] := DialogUnitToPixelX(DialogRegEx.Match[2]);
  DialogData.Integers['top'] := DialogUnitToPixelY(DialogRegEx.Match[3]);
  DialogData.Integers['width'] := DialogUnitToPixelX(DialogRegEx.Match[4]);
  DialogData.Integers['height'] := DialogUnitToPixelY(DialogRegEx.Match[5]);
end;

procedure TDialogResourceParser.ParseControls(DialogData: TJSONObject);
var
  i: Integer;
  ControlsData: TJSONArray;
  ControlData: TJSONObject;
  Line: String;
begin
  ControlsData := TJSONArray.Create;
  for i := 1 to FSource.Count - 1 do
  begin
    Line := FSource[i];
    if FControlExpr.Exec(Line) then
    begin
      ControlData := TJSONObject.Create([
        'type', FControlExpr.Match[1],
        'caption', FControlExpr.Match[2],
        'id', StrToInt(FControlExpr.Match[3]),
        'left', DialogUnitToPixelX(FControlExpr.Match[4]),
        'top', DialogUnitToPixelY(FControlExpr.Match[5]),
        'width', DialogUnitToPixelX(FControlExpr.Match[6]),
        'height', DialogUnitToPixelY(FControlExpr.Match[7]),
        'style', FControlExpr.Match[9]
      ]);
      ControlsData.Add(ControlData);
    end
    else if FGenericControlExpr.Exec(Line) then
    begin
      ControlData := TJSONObject.Create([
        'type', FGenericControlExpr.Match[3],
        'caption', FGenericControlExpr.Match[1],
        'id', StrToInt(FGenericControlExpr.Match[2]),
        'left', DialogUnitToPixelX(FGenericControlExpr.Match[5]),
        'top', DialogUnitToPixelY(FGenericControlExpr.Match[6]),
        'width', DialogUnitToPixelX(FGenericControlExpr.Match[7]),
        'height', DialogUnitToPixelY(FGenericControlExpr.Match[8]),
        'style', FGenericControlExpr.Match[4]
      ]);
      ControlsData.Add(ControlData);
    end;
  end;
  DialogData.Add('controls', ControlsData);
end;

constructor TDialogResourceParser.Create(Source: TStrings);
begin
  FSource := Source;
  FControlExpr := TRegExpr.Create;
  //RegExpr does not suuport non capturing group
  //FControlExpr.Expression := '\s*(\w*)\s*\"([^"]*)\",\s(\d+),\s(\d+),\s(\d+),\s(\d+),\s(\d+)(?:,\s(.+))*';
  FControlExpr.Expression := '\s*(\w*)\s*\"([^"]*)\",\s(\d+),\s(\d+),\s(\d+),\s(\d+),\s(\d+)(,\s(.+))*';
  FControlExpr.Compile;
  FGenericControlExpr := TRegExpr.Create;
  FGenericControlExpr.Expression := '\s*CONTROL\s*\"([^"]*)\",\s(\d+),\s\"([^"]*)\",([^,]*),\s(\d+),\s(\d+),\s(\d+),\s(\d+)';
  FGenericControlExpr.Compile;
end;

destructor TDialogResourceParser.Destroy;
begin
  FControlExpr.Destroy;
  FGenericControlExpr.Destroy;
  inherited Destroy;
end;

function TDialogResourceParser.Parse: TJSONObject;
begin
  if FSource = nil then
  begin
    Result := nil;
    raise Exception.Create('FSource = nil');
  end;
  Result := TJSONObject.Create;
  try
    ParseDialogInfo(Result);
    ParseControls(Result);
  except
     on E: Exception do
     begin
       FreeAndNil(Result);
       raise;
     end;
  end;
end;

end.

