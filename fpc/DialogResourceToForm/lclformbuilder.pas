unit LCLFormBuilder;

{$mode objfpc}{$H+}
{$MODESWITCH ADVANCEDRECORDS}

interface

uses
  Classes, SysUtils;

type

  { TFormInfo }

  TFormInfo = record
    Caption: String;
    ClassName: String;
    Top: Integer;
    Left: Integer;
    Width: Integer;
    Height: Integer;
    function VariableName: String;
    procedure SetCaption(const Value: String);
    procedure SetClassName(const Value: String);
    procedure SetLeft(Value: Integer);
    procedure SetTop(Value: Integer);
    procedure SetHeight(Value: Integer);
    procedure SetWidth(Value: Integer);
  end;

  { TCustomLCLFormBuilder }

  TCustomLCLFormBuilder = class
  private
    FFormInfo: TFormInfo;
    FPascalFile: TStringList;
    FLFMFile: TStringList;
    FWorkingFile: TStringList;
    FIndentation: Integer;
  protected
    function GetUsedUnits: String; virtual;
    procedure IncIndentation;
    procedure DecIndentation;
    procedure Prepare; virtual;
    procedure WriteLine(const Line: String);
    procedure WriteLine(const Line: String; const Args: array of const);
    procedure WriteLines(const Lines: array of String);
    procedure WriteLFMBody; virtual;
    procedure WritePascalBody; virtual;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    property FormInfo: TFormInfo read FFormInfo write FFormInfo;
    procedure Save(const Directory, AUnitName: String);
  end;

implementation

{ TFormInfo }

function TFormInfo.VariableName: String;
begin
  Result := Copy(ClassName, 2, Length(ClassName) - 1);
end;

procedure TFormInfo.SetCaption(const Value: String);
begin
  Caption := Value;
end;

procedure TFormInfo.SetClassName(const Value: String);
begin
  ClassName := Value;
end;

procedure TFormInfo.SetLeft(Value: Integer);
begin
  Left := Value;
end;

procedure TFormInfo.SetTop(Value: Integer);
begin
  Top := Value;
end;

procedure TFormInfo.SetHeight(Value: Integer);
begin
  Height := Value;
end;

procedure TFormInfo.SetWidth(Value: Integer);
begin
  Width := Value;
end;


{ TCustomLCLFormBuilder }

function TCustomLCLFormBuilder.GetUsedUnits: String;
begin
  Result := 'Classes, SysUtils, FileUtil, Forms, Controls, Graphics';
end;

procedure TCustomLCLFormBuilder.IncIndentation;
begin
  Inc(FIndentation, 2);
end;

procedure TCustomLCLFormBuilder.DecIndentation;
begin
  Dec(FIndentation, 2);
end;

procedure TCustomLCLFormBuilder.Prepare;
begin
  //
end;

procedure TCustomLCLFormBuilder.WriteLine(const Line: String);
begin
  FWorkingFile.Add(Space(FIndentation) + Line);
end;

procedure TCustomLCLFormBuilder.WriteLine(const Line: String; const Args: array of const);
begin
  WriteLine(Format(Line, Args));
end;

procedure TCustomLCLFormBuilder.WriteLines(const Lines: array of String);
var
  i: Integer;
begin
  for i := Low(Lines) to High(Lines) do
    WriteLine(Lines[i]);
end;

procedure TCustomLCLFormBuilder.WriteLFMBody;
begin
  //
end;

procedure TCustomLCLFormBuilder.WritePascalBody;
begin
  //
end;

constructor TCustomLCLFormBuilder.Create;
begin
  FLFMFile := TStringList.Create;
  FPascalFile := TStringList.Create;
  FIndentation := 2;
  FFormInfo.ClassName := 'TMyForm';
  FFormInfo.Left := 100;
  FFormInfo.Top := 100;
  FFormInfo.Height := 300;
  FFormInfo.Width := 400;
end;

destructor TCustomLCLFormBuilder.Destroy;
begin
  FLFMFile.Destroy;
  FPascalFile.Destroy;
  inherited Destroy;
end;

procedure TCustomLCLFormBuilder.Save(const Directory, AUnitName: String);
begin
  FPascalFile.Clear;
  FLFMFile.Clear;
  Prepare;
  FWorkingFile := FPascalFile;
  FPascalFile.Clear;
  FPascalFile.Add(Format('unit %s;', [AUnitName]) + LineEnding);
  FPascalFile.Add('interface' + LineEnding);
  FPascalFile.Add('uses');
  FPascalFile.Add('  ' + GetUsedUnits + ';');
  FPascalFile.Add('');
  FPascalFile.Add('type');
  FPascalFile.Add('');
  FPascalFile.Add(Format('  %s = class(TForm)', [FFormInfo.ClassName]));

  FIndentation := 4;
  WritePascalBody;

  FPascalFile.Add('  private');
  FPascalFile.Add('  public');
  FPascalFile.Add('  end;');
  FPascalFile.Add('');
  FPascalFile.Add('var');
  FPascalFile.Add(Format('  %s: %s;', [FFormInfo.VariableName, FFormInfo.ClassName]));
  FPascalFile.Add('');
  FPascalFile.Add('implementation');
  FPascalFile.Add('');
  FPascalFile.Add('{$R *.lfm}');
  FPascalFile.Add('');
  FPascalFile.Add('end.');
  FPascalFile.SaveToFile(IncludeTrailingPathDelimiter(Directory) + AUnitName + '.pas');

  FWorkingFile := FLFMFile;
  FLFMFile.Clear;
  FLFMFile.Add(Format('object %s: %s', [FFormInfo.VariableName, FFormInfo.ClassName]));
  FLFMFile.Add(Format('  Caption = ''%s''', [FFormInfo.Caption]));
  FLFMFile.Add(Format('  Left = %d', [FFormInfo.Left]));
  FLFMFile.Add(Format('  Height = %d', [FFormInfo.Height]));
  FLFMFile.Add(Format('  Top = %d', [FFormInfo.Top]));
  FLFMFile.Add(Format('  Width = %d', [FFormInfo.Width]));
  FLFMFile.Add('  LCLVersion = ''1.7''');

  FIndentation := 2;
  WriteLFMBody;

  FLFMFile.Add('end');
  FLFMFile.SaveToFile(IncludeTrailingPathDelimiter(Directory) + AUnitName + '.lfm');
end;


end.

