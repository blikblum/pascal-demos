program dialog2json;

{$mode objfpc}{$H+}

uses
  Classes, fpjson, DialogResourceParser;

var
  Parser: TDialogResourceParser;
  Source: TStrings;
  Data: TJSONObject;

begin

  if Paramcount = 0 then
  begin
    WriteLn('Needs a filename param');
    Exit;
  end;

  Source := TStringList.Create;
  Parser := TDialogResourceParser.Create(Source);
  try
    Source.LoadFromFile(ParamStr(1));
    Data := Parser.Parse;
    WriteLn(Data.FormatJSON());
  finally
    Parser.Destroy;
    Source.Destroy;
  end;

end.

