program dialog2form;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes, SysUtils, DialogResourceParser, LCLFormBuilder, DialogToFormConverter,
  CustApp, fpjson;

type

  { TDialogConverterApplication }

  TDialogConverterApplication = class(TCustomApplication)
  private
    FFileList: TStrings;
  protected
    procedure DoRun; override;
    procedure ConvertFiles;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteHelp; virtual;
  end;

{ TDialogConverterApplication }

procedure TDialogConverterApplication.DoRun;
var
  ErrorMsg: String;
begin
  // quick check parameters
  ErrorMsg := CheckOptions('h', ['help'], nil, FFileList);
  if ErrorMsg <> '' then begin
    ShowException(Exception.Create(ErrorMsg));
    Terminate;
    Exit;
  end;

  if (FFileList.Count = 0) then
  begin
    WriteLn('Is necessary to specify the dialog resource file');
    Terminate;
    Exit;
  end;

  // parse parameters
  if HasOption('h', 'help') then begin
    WriteHelp;
    Terminate;
    Exit;
  end;

  { add your program here }
  ConvertFiles;

  // stop program loop
  Terminate;
end;

procedure TDialogConverterApplication.ConvertFiles;
var
  i: Integer;
  SrcFileName: String;
  DialogData: TJSONObject;
  Parser: TDialogResourceParser;
  SrcFile: TStringList;
begin
  for i := 0 to FFileList.Count - 1 do
  begin
    DialogData := nil;
    SrcFileName := ExtractFileName(FFileList[i]);
    SrcFile := TStringList.Create;
    SrcFile.LoadFromFile(SrcFileName);
    Parser := TDialogResourceParser.Create(SrcFile);
    try
      DialogData := Parser.Parse;
      ConvertDialogToForm(DialogData, ExtractFilePath(ParamStr(0)), ChangeFileExt(SrcFileName, ''));
    finally
      DialogData.Free;
      Parser.Destroy;
      SrcFile.Destroy;
    end;
  end;
end;

constructor TDialogConverterApplication.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException := True;
  FFileList := TStringList.Create;
end;

destructor TDialogConverterApplication.Destroy;
begin
  FFileList.Free;
  inherited Destroy;
end;

procedure TDialogConverterApplication.WriteHelp;
begin
  { add your help code here }
  writeln('Usage: ', ExeName, ' -h');
end;

var
  Application: TDialogConverterApplication;
begin
  Application := TDialogConverterApplication.Create(nil);
  Application.Title := 'Dialog To Form Converter';
  Application.Run;
  Application.Free;
end.

