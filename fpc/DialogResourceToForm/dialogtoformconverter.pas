unit DialogToFormConverter;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpjson;

procedure ConvertDialogToForm(DialogData: TJSONObject; const Directory, AUnitName: String);


implementation

uses
  LCLFormBuilder;

type

  { TDialogFormBuilder }

  TDialogFormBuilder = class(TCustomLCLFormBuilder)
  private
    FDialogData: TJSONObject;
    FControlsData: TJSONArray;
    FClassCountData: TJSONObject;
    function GetControlName(const ControlClass: String): String;
  protected
    procedure Prepare; override;
    procedure WriteLFMBody; override;
    procedure WritePascalBody; override;
  public
    constructor Create; override;
    destructor Destroy; override;
    property DialogData: TJSONObject read FDialogData write FDialogData;
  end;

  StringArray = array of String;

  { TControlMapper }

  TControlMapper = class
  public
    class function ControlClass: String; virtual;
    class function Caption(const PristineCaption, {%H-}ControlType: String): String; virtual;
    class function CaptionProperty: String; virtual;
    class function ExtraProperties: StringArray; virtual;
  end;

  TControlMapperClass = class of TControlMapper;

  { TRadioButtonMapper }

  TRadioButtonMapper = class(TControlMapper)
  end;

  TCheckBoxMapper = class(TControlMapper)
  end;

  { TEditMapper }

  TEditMapper = class(TControlMapper)
  public
    class function CaptionProperty: String; override;
  end;

  { TComboBoxMapper }

  TComboBoxMapper = class(TControlMapper)
  public
    class function CaptionProperty: String; override;
  end;

  { TListBoxMapper }

  TListBoxMapper = class(TControlMapper)
  public
    class function CaptionProperty: String; override;
  end;

  TGroupBoxMapper = class(TControlMapper)
  end;

  TButtonMapper = class(TControlMapper)
  end;

  { TListViewMapper }

  TListViewMapper = class(TControlMapper)
  public
    class function CaptionProperty: String; override;
  end;

  { TLabelMapper }

  TLabelMapper = class(TControlMapper)
  public
    class function ExtraProperties: StringArray; override;
  end;

  { TRightLabelMapper }

  TRightLabelMapper = class(TLabelMapper)
  public
    class function ControlClass: String; override;
    class function ExtraProperties: StringArray; override;
  end;

  { TCenterLabelMapper }

  TCenterLabelMapper = class(TLabelMapper)
  public
    class function ControlClass: String; override;
    class function ExtraProperties: StringArray; override;
  end;

  { TUnknowMapper }

  TUnknowMapper = class(TControlMapper)
    class function ControlClass: String; override;
    class function Caption(const PristineCaption, ControlType: String): String; override;
  end;

function GetControlMapper(const ControlType: String): TControlMapperClass;
begin
  case ControlType of
    'AUTORADIOBUTTON': Result := TRadioButtonMapper;
    'AUTOCHECKBOX': Result := TCheckBoxMapper;
    'EDITTEXT': Result := TEditMapper;
    'COMBOBOX': Result := TComboBoxMapper;
    'LISTBOX': Result := TListBoxMapper;
    'GROUPBOX': Result := TGroupBoxMapper;
    'LTEXT': Result := TLabelMapper;
    'CTEXT': Result := TCenterLabelMapper;
    'RTEXT': Result := TRightLabelMapper;
    'PUSHBUTTON': Result := TButtonMapper;
    'SysListView32': Result := TListViewMapper;
  else
    Result := TUnknowMapper;
  end;
end;

procedure ConvertDialogToForm(DialogData: TJSONObject; const Directory, AUnitName: String);
var
  Builder: TDialogFormBuilder;
begin
  Builder := TDialogFormBuilder.Create;
  try
    Builder.DialogData := DialogData;
    Builder.Save(Directory, AUnitName);
  finally
    Builder.Destroy;
  end;
end;

{ TListBoxMapper }

class function TListBoxMapper.CaptionProperty: String;
begin
  Result := '';
end;

{ TListViewMapper }

class function TListViewMapper.CaptionProperty: String;
begin
  Result := '';
end;

{ TEditMapper }

class function TEditMapper.CaptionProperty: String;
begin
  Result := 'Text';
end;

{ TComboBoxMapper }

class function TComboBoxMapper.CaptionProperty: String;
begin
  Result := 'Text';
end;

{ TUnknowMapper }

class function TUnknowMapper.ControlClass: String;
begin
  Result := 'TPanel';
end;

class function TUnknowMapper.Caption(const PristineCaption, ControlType: String): String;
begin
  Result := PristineCaption + Format(' [Unknown Type: "%s"]', [ControlType]);
end;

{ TLabelMapper }

class function TLabelMapper.ExtraProperties: StringArray;
var
  Res: array[0..0] of String = ('AutoSize = False');
begin
  Result := Res;
end;

{ TCenterLabelMapper }

class function TCenterLabelMapper.ControlClass: String;
begin
  Result := 'TLabel';
end;

class function TCenterLabelMapper.ExtraProperties: StringArray;
var
  Res: array[0..1] of String = ('AutoSize = False', 'Alignment = taCenterJustify');
begin
  Result := Res;
end;

{ TRightLabelMapper }

class function TRightLabelMapper.ControlClass: String;
begin
  Result := 'TLabel';
end;

class function TRightLabelMapper.ExtraProperties: StringArray;
var
  Res: array[0..1] of String = ('AutoSize = False', 'Alignment = taRightJustify');
begin
  Result := Res;
end;

{ TControlMapper }

class function TControlMapper.ControlClass: String;
var
  PosSuffix: Integer;
begin
  Result := '';
  PosSuffix := Pos('Mapper', ClassName);
  if PosSuffix > 0 then
    Result := Copy(ClassName, 1, PosSuffix - 1);
end;

class function TControlMapper.Caption(const PristineCaption, ControlType: String): String;
begin
  Result := PristineCaption;
end;

class function TControlMapper.CaptionProperty: String;
begin
  Result := 'Caption';
end;

class function TControlMapper.ExtraProperties: StringArray;
begin
  Result := nil;
end;

{ TDialogFormBuilder }

function TDialogFormBuilder.GetControlName(const ControlClass: String): String;
var
  ControlClassCount: Integer;
begin
  ControlClassCount := FClassCountData.Get(ControlClass, 0);
  Inc(ControlClassCount);
  Result := ControlClass + IntToStr(ControlClassCount);
  FClassCountData.Integers[ControlClass] := ControlClassCount;
end;

procedure TDialogFormBuilder.Prepare;
var
  i: Integer;
  ControlData: TJSONObject;
  Mapper: TControlMapperClass;
begin
  FreeAndNil(FControlsData);
  FControlsData := FDialogData.Arrays['controls'].Clone as TJSONArray;
  for i := 0 to FControlsData.Count - 1 do
  begin
    ControlData := FControlsData.Objects[i];
    Mapper := GetControlMapper(ControlData.Get('type', ''));
    ControlData.Strings['class'] := Mapper.ControlClass;
    ControlData.Strings['name'] := GetControlName(Mapper.ControlClass);
  end;
  FormInfo.SetCaption(DialogData.Get('name', ''));
  FormInfo.SetClassName(Format('T%sForm', [DialogData.Get('name', '')]));
  FormInfo.SetLeft(DialogData.Get('left', FormInfo.Left));
  FormInfo.SetTop(DialogData.Get('left', FormInfo.Left));
  FormInfo.SetWidth(DialogData.Get('width', FormInfo.Width));
  FormInfo.SetHeight(DialogData.Get('height', FormInfo.Height));
end;

procedure TDialogFormBuilder.WriteLFMBody;
var
  i: Integer;
  ControlData: TJSONObject;
  Mapper: TControlMapperClass;
  CaptionProp, Caption: String;
begin
  for i := 0 to FControlsData.Count -1 do
  begin
    ControlData := FControlsData.Objects[i];
    Mapper := GetControlMapper(ControlData.Get('type', ''));
    WriteLine('object %s: %s', [ControlData.Get('name', ''), ControlData.Get('class', '')]);
    IncIndentation;
    WriteLine('Left = %d', [ControlData.Get('left', 0)]);
    WriteLine('Top = %d', [ControlData.Get('top', 0)]);
    WriteLine('Width = %d', [ControlData.Get('width', 0)]);
    WriteLine('Height = %d', [ControlData.Get('height', 0)]);
    CaptionProp := Mapper.CaptionProperty;
    Caption := Mapper.Caption(ControlData.Get('caption', ''), ControlData.Get('type', ''));
    if (CaptionProp <> '') and (Caption <> '') then
      WriteLine('%s = ''%s''', [CaptionProp, Caption]);
    WriteLines(Mapper.ExtraProperties);
    DecIndentation;
    WriteLine('end');
  end;
end;

procedure TDialogFormBuilder.WritePascalBody;
var
  i: Integer;
  ControlData: TJSONObject;
begin
  for i := 0 to FControlsData.Count -1 do
  begin
    ControlData := FControlsData.Objects[i];
    WriteLine('%s: %s;', [ControlData.Get('name', ''), ControlData.Get('class', '')]);
  end;
end;

constructor TDialogFormBuilder.Create;
begin
  inherited Create;
  FClassCountData := TJSONObject.Create;
end;

destructor TDialogFormBuilder.Destroy;
begin
  FClassCountData.Destroy;
  FControlsData.Free;
  inherited Destroy;
end;


end.

