program OLEEditTextDoc;

{$mode objfpc}{$H+}

uses
  Classes, sysutils, variants, comobj;

function FileNameToUrl(const FileName: String): WideString;
begin
  Result := Format('file:///%s', [StringReplace(ExpandFileName(FileName), '\', '/', [rfReplaceAll])]);
end;

function CreateStruct(const Reflection: Variant; const strTypeName: WideString): Variant;
var
  IdlClass: Variant;
begin
  IdlClass := Reflection.forName(strTypeName);
  // https://api.libreoffice.org/docs/idl/ref/interfacecom_1_1sun_1_1star_1_1reflection_1_1XIdlClass.html
  IdlClass.createObject(Result);
end;

procedure DumpTableInfo(Table: Variant);
var
  TableRows, TableColumns, Cursor, Cell: Variant;
  CellStrList: TStringList;
  CellName: WideString;
begin
  WriteLn(' # Table ', Table.getName());
  TableRows := Table.GetRows();
  TableColumns := Table.GetColumns();
  WriteLn('Row count: ', TableRows.GetCount(), ' - Column count: ', TableColumns.GetCount());
  CellStrList := TStringList.Create;
  try
    Cursor := Table.CreateCursorByCellName(WideString('A1'));
    repeat
      CellName := Cursor.getRangeName();
      // cursor goes to another row write the current row info and reset the list
      if (CellName[1] = 'A') and (CellStrList.Count > 0) then
      begin
        WriteLn(CellStrList.DelimitedText);
        CellStrList.Clear;
      end;
      Cell := Table.getCellByName(CellName);
      CellStrList.Add(Format('[%s=%s]', [CellName, Cell.GetString()]));
    until not Cursor.GoRight(1, False);
    WriteLn(CellStrList.DelimitedText);
  finally
    CellStrList.Destroy;
  end;
end;

procedure CopyTableRow(Table: Variant; RowIndex, CopyCount: Integer);
var
  TableRows, Cursor, Cell, InsertCell: Variant;
  RowIndexStr, ColumnTemplate, ColumnText, CellName, InsertCellName: WideString;
  i: Integer;
  FirstPass: Boolean;
begin
  TableRows := Table.GetRows();
  TableRows.InsertByIndex(RowIndex, CopyCount);
  RowIndexStr := IntToStr(RowIndex);
  CellName := WideString('A' + RowIndexStr);
  Cursor := Table.CreateCursorByCellName(CellName);
  FirstPass := True;
  repeat
    // walk by all columns in the row to be copied...
    CellName := Cursor.getRangeName();
    if not FirstPass and (CellName <> '') and (CellName[1] = 'A') then
      Break;
    FirstPass := False;
    Cell := Table.getCellByName(CellName);
    ColumnText := Cell.GetString();
    // ...if is not empty create a column template
    if ColumnText <> '' then
    begin
      ColumnTemplate := StringReplace(CellName, RowIndexStr, '%d', []);
      // ...and walk into the inserted rows populating the column
      for i := RowIndex to RowIndex + CopyCount do
      begin
        InsertCellName := Format(ColumnTemplate, [i + 1]);
        InsertCell := Table.getCellByName(InsertCellName);
        if not VarIsClear(InsertCell) then
          InsertCell.SetString(ColumnText);
      end;
    end;
  until not Cursor.GoRight(1, False);
end;

const
  ServiceName = 'com.sun.star.ServiceManager';

var
  ServiceManager, Desktop, Document, Reflection, LoadParams: Variant;
  xText, xTextCursor: Variant;
  xAllTables, xTable: Variant;
  xSearchDescriptor, xSearchResult, xReplaceDescr: Variant;
  i: Integer;

begin
  if Assigned(InitProc) then
    TProcedure(InitProc);

  try
    ServiceManager := CreateOleObject(ServiceName);
  except
    WriteLn('Unable to start OO/LO.');
    Exit;
  end;

  // https://api.libreoffice.org/docs/idl/ref/servicecom_1_1sun_1_1star_1_1frame_1_1Desktop.html
  Desktop := ServiceManager.CreateInstance('com.sun.star.frame.Desktop');
  Reflection := ServiceManager.CreateInstance('com.sun.star.reflection.CoreReflection');

  LoadParams := VarArrayCreate([0, 2], varVariant);
  LoadParams[0] := CreateStruct(Reflection, 'com.sun.star.beans.PropertyValue');
  LoadParams[0].Name := 'AsTemplate';
  LoadParams[0].Value := True;
  LoadParams[1] := CreateStruct(Reflection, 'com.sun.star.beans.PropertyValue');
  LoadParams[1].Name := 'SuggestedSaveAsName';
  LoadParams[1].Value := 'New File';

  // Desktop implements XComponentLoader https://api.libreoffice.org/docs/idl/ref/interfacecom_1_1sun_1_1star_1_1frame_1_1XComponentLoader.html
  // Document is a TextDocument https://api.libreoffice.org/docs/idl/ref/servicecom_1_1sun_1_1star_1_1text_1_1TextDocument.html
  Document := Desktop.LoadComponentFromURL(FileNameToUrl('sample.odt'), '_blank', 0, LoadParams);

  // insert some text
  xText := Document.GetText;

  xTextCursor := xText.CreateTextCursor;

  xText.InsertString(xTextCursor, WideString('Editing document at ' + DateTimeToStr(Date)), False);


  // search with regular expression
  // Document implements XSearchable https://api.libreoffice.org/docs/idl/ref/interfacecom_1_1sun_1_1star_1_1util_1_1XSearchable.html
  xSearchDescriptor := Document.CreateSearchDescriptor();
  xSearchDescriptor.SetSearchString('\[.+\]');
  xSearchDescriptor.SearchRegularExpression := True;

  xSearchResult := Document.FindFirst(xSearchDescriptor);
  while not VarIsClear(xSearchResult) do
  begin
    WriteLn('found tag ', xSearchResult.getString());
    xSearchResult := Document.FindNext(xSearchResult.getEnd(), xSearchDescriptor);
  end;

  // manual replace
  xSearchDescriptor := Document.CreateSearchDescriptor();
  xSearchDescriptor.SetSearchString('[name]');
  xSearchResult := Document.FindFirst(xSearchDescriptor);
  if not VarIsClear(xSearchResult) then
    xSearchResult.SetString(WideString('The Name (replaced manually)'));

  // replace all

  xReplaceDescr := Document.CreateReplaceDescriptor();
  xReplaceDescr.setSearchString('[hospitalname]');
  xReplaceDescr.setReplaceString('HGRS (replaced by Document service)');
  Document.replaceAll(xReplaceDescr);

  // manipulate table

  // https://api.libreoffice.org/docs/idl/ref/servicecom_1_1sun_1_1star_1_1text_1_1TextTables.html
  xAllTables := Document.getTextTables();
  for i := 0 to xAllTables.GetCount() - 1 do
  begin
    xTable := xAllTables.GetByIndex(i);
    DumpTableInfo(xTable);
  end;

  xTable := xAllTables.GetByIndex(0);
  CopyTableRow(xTable, 6, 2);
end.

