program StreamGenericsToJSON;

{$mode objfpc}{$H+}

uses
  Classes, fgl, fpjsonrtti, fpjson, typinfo;

type

  { TPerson }

  TPerson = class
  private
    FName: String;
  public
    constructor Create(const AName: String);
  published
    property Name: String read FName write FName;
  end;

  TPeople = specialize TFPGList<TPerson>;

  { TGroup }

  TGroup = class
  private
    FName: String;
    FPeople: TPeople;
    FStreamer: TJSONStreamer;
    procedure StreamProperty(Sender : TObject; AObject : TObject; Info : PPropInfo; var Res : TJSONData);
    procedure SetPeople(AValue: TPeople);
  public
    constructor Create;
    destructor Destroy; override;
    function ToJSON: String;
  published
    property Name: String read FName write FName;
    property People: TPeople read FPeople write SetPeople;
  end;

constructor TPerson.Create(const AName: String);
begin
  FName := AName;
end;

{ TGroup }

procedure TGroup.StreamProperty(Sender: TObject; AObject: TObject;
  Info: PPropInfo; var Res: TJSONData);
var
  i: Integer;
  ListData: TJSONArray;
  ObjData: TJSONObject;
  ThePeople: TPeople;
begin
  if (Info^.PropType^.Kind = tkClass) and GetTypeData(Info^.PropType)^.ClassType.InheritsFrom(TPeople) then
  begin
    ListData := TJSONArray.Create;
    ThePeople := GetObjectProp(AObject, Info) as TPeople;
    for i := 0 to ThePeople.Count - 1 do
    begin
      ObjData := TJSONStreamer(Sender).ObjectToJSON(ThePeople.Items[i]);
      ListData.Add(ObjData);
    end;
    Res := ListData;
  end;
end;

procedure TGroup.SetPeople(AValue: TPeople);
begin
  if FPeople = AValue then Exit;
  FPeople := AValue;
end;

constructor TGroup.Create;
begin
  FPeople := TPeople.Create;
  FStreamer := TJSONStreamer.Create(nil);
  FStreamer.OnStreamProperty := @StreamProperty;
end;

destructor TGroup.Destroy;
begin
  FStreamer.Destroy;
  FPeople.Destroy;
  inherited Destroy;
end;

function TGroup.ToJSON: String;
begin
  Result := FStreamer.ObjectToJSONString(Self);
end;

var
  Group: TGroup;
begin
  Group := TGroup.Create;
  Group.Name := 'My Group';
  Group.People.Add(TPerson.Create('Luiz'));
  Group.People.Add(TPerson.Create('Fernando'));
  WriteLn(Group.ToJSON);
  Group.Destroy;
end.

