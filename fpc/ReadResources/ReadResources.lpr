program ReadResources;

{$mode objfpc}{$H+}
{$R original.res}

uses
  Classes;

function EnumNames(ModuleHandle : TFPResourceHMODULE; ResourceType, ResourceName : PChar; lParam : PtrInt) : LongBool; stdcall;
begin
  WriteLn(ResourceName);
  Result := True;
end;

function EnumTypes(ModuleHandle : TFPResourceHMODULE; ResourceType : PChar; lParam : PtrInt) : LongBool; stdcall;
begin
  EnumResourceNames(ModuleHandle, ResourceType, @EnumNames, 0);
  Result := True;
end;


begin
  EnumResourceTypes(HINSTANCE, @EnumTypes, 0);
end.

