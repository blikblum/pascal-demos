program CollectionNotify;

{$mode objfpc}{$H+}

uses
  Classes;

type

  { TMyCollection }

  TMyCollection = class(TCollection)
  public
    procedure Notify(Item: TCollectionItem; Action: TCollectionNotification); override;
    procedure Update(Item: TCollectionItem); override;
  end;

{ TMyCollection }

procedure TMyCollection.Notify(Item: TCollectionItem; Action: TCollectionNotification);
var
  LastItem: TCollectionItem;
begin
  inherited Notify(Item, Action);
  if Action = cnAdded then
  begin
    LastItem := Items[Count - 1];
    if LastItem.Collection = nil then
      WriteLn('Notify added - Item.Collection = nil')
    else
      WriteLn('Notify added - Item.Collection <> nil');
  end;
end;

procedure TMyCollection.Update(Item: TCollectionItem);
var
  LastItem: TCollectionItem;
begin
  inherited Update(Item);
  LastItem := Items[Count - 1];
  if LastItem.Collection = nil then
    WriteLn('Update - Item.Collection = nil')
  else
    WriteLn('Update - Item.Collection <> nil');
end;

var
  Collection: TMyCollection;

begin
  Collection := TMyCollection.Create(TCollectionItem);
  Collection.Add;
  Collection.Destroy;
end.

