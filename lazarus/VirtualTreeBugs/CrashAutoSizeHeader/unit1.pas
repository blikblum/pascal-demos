unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls, VirtualTrees;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    VirtualStringTree1: TVirtualStringTree;
    procedure Button1Click(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
  VirtualStringTree1.Header.Columns.Add;
end;

end.

