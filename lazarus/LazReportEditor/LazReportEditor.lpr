program LazReportEditor;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  LCLProc, sysutils, Dialogs,
   fMain, mReport;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TReportModule, ReportModule);
  //DebugLn(IntToStr(Paramcount));
  //DebugLn(ParamStr(1));
  if ParamCount > 0 then
  begin
    if ReportModule.EditReport(ParamStr(1)) then
      Application.Terminate
    else
      ShowMessage(Format('Unable to load "%s"', [ParamStr(1)]));
  end;
  if not Application.Terminated then
    Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.

