unit fMain; 

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs,
  EditBtn, StdCtrls, FileCtrl, Buttons, LR_Class;

type

  { TMainForm }

  TMainForm = class(TForm)
    DirectoryEdit: TDirectoryEdit;
    FileListBox: TFileListBox;
    Label1: TLabel;
    RefreshButton: TSpeedButton;
    procedure DirectoryEditAcceptDirectory(Sender: TObject; var Value: String);
    procedure FileListBoxDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RefreshButtonClick(Sender: TObject);
  private
  public
    { public declarations }
  end; 

var
  MainForm: TMainForm;

implementation

{$r *.lfm}

uses
  LazUTF8, LazFileUtils, mReport;


{ TMainForm }
procedure TMainForm.FileListBoxDblClick(Sender: TObject);
var
  i: Integer;
begin
  i := FileListBox.ItemIndex;
  if i <> -1 then
    ReportModule.EditReport(FileListBox.FileName);
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  DirectoryEdit.Directory := ExtractFileDir(ParamStrUTF8(0));
  FileListBox.Directory := DirectoryEdit.Directory;
end;

procedure TMainForm.RefreshButtonClick(Sender: TObject);
begin
  FileListBox.UpdateFileList;
end;

procedure TMainForm.DirectoryEditAcceptDirectory(Sender: TObject;
  var Value: String);
begin
  if Value <> FileListBox.Directory then
    FileListBox.Directory := Value
  else
    FileListBox.UpdateFileList;
end;

end.

