unit mReport;

{$mode objfpc}

interface

uses
  Classes, SysUtils, Forms, Controls, Dialogs, LR_Class, LR_Desgn;

type

  { TReportModule }

  TReportModule = class(TDataModule)
    Report: TfrReport;
  private
    { private declarations }
  public
    { public declarations }
    function EditReport(const FileName: String): Boolean;
  end; 

var
  ReportModule: TReportModule;

implementation

{$R *.lfm}

uses
  LazFileUtils;

function TReportModule.EditReport(const FileName: String): Boolean;
begin
  if not FileExistsUTF8(FileName) then
    Exit(False);
  Result := True;
  try
    Report.LoadFromFile(FileName);
    Report.DesignReport;
  except
    Result := False;
  end;
end;

end.

