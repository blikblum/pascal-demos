unit Main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TMainForm }

  TMainForm = class(TForm)
    UpdateButton: TButton;
    DumpButton: TButton;
    InfoMemo: TMemo;
    procedure DumpButtonClick(Sender: TObject);
    procedure UpdateButtonClick(Sender: TObject);
  private
    { private declarations }
    FMonitors: TMonitorList;
  public
    { public declarations }
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
  end;

var
  MainForm: TMainForm;

implementation

uses
  LCLType, Types, LCLIntf;

{$R *.lfm}

type
  TMonitorHack = class(TObject)
  private
    FHandle: HMONITOR;
    FMonitorNum: Integer;
  end;

function EnumMonitors(hMonitor: HMONITOR; hdcMonitor: HDC; lprcMonitor: PRect;
    dwData: LPARAM): LongBool; stdcall;
var
  Monitors: TMonitorList absolute dwData;
  Monitor: TMonitor;
begin
  Monitor := TMonitor.Create;
  TMonitorHack(Monitor).FHandle := hMonitor;
  Monitors.Add(Monitor);
  Result := True;
end;

{ TMainForm }

procedure TMainForm.DumpButtonClick(Sender: TObject);
var
  i: Integer;
begin
  InfoMemo.Lines.Add('Monitor count: ' + IntToStr(FMonitors.Count));
  for i := 0 to FMonitors.Count - 1 do
    InfoMemo.Lines.Add('  %d - %d', [i, FMonitors.Items[i].Handle]);
end;

procedure TMainForm.UpdateButtonClick(Sender: TObject);
begin
  FMonitors.Clear;
  EnumDisplayMonitors(0, nil, @EnumMonitors, LParam(FMonitors));
end;

constructor TMainForm.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  FMonitors := TMonitorList.Create;
end;

destructor TMainForm.Destroy;
begin
  FMonitors.Destroy;
  inherited Destroy;
end;

end.

