unit MainView;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, Spin, ColorBox;

type

  { TMainForm }

  TMainForm = class(TForm)
    BlendColorBox: TColorBox;
    Label7: TLabel;
    TargetGroupBox: TGroupBox;
    TargetYEdit: TSpinEdit;
    BitmapWidthEdit: TSpinEdit;
    BitmapGroupBox: TGroupBox;
    TargetXEdit: TSpinEdit;
    Label1: TLabel;
    Label2: TLabel;
    AlphaBlendPaintBox: TPaintBox;
    FillRectPaintBox: TPaintBox;
    BitmapHeightEdit: TSpinEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    procedure AlphaBlendPaintBoxPaint(Sender: TObject);
    procedure FillRectPaintBoxPaint(Sender: TObject);
  private

  public

  end;

var
  MainForm: TMainForm;

implementation

uses
  LCLIntf, AlphaBlendImpl, LCLType;

{$R *.lfm}

{ TMainForm }

procedure TMainForm.AlphaBlendPaintBoxPaint(Sender: TObject);
var
  ControlRect, BitmapRect: TRect;
  Target: TPoint;
  Box: TPaintBox absolute Sender;
  Bitmap: TBitmap;
begin
  ControlRect := Box.ClientRect;
  Box.Canvas.Brush.Color := clYellow;
  Box.Canvas.FillRect(ControlRect);

  BitmapRect := Rect(0, 0, BitmapWidthEdit.Value, BitmapHeightEdit.Value);
  Bitmap := TBitmap.Create;
  Bitmap.PixelFormat := pf32bit;
  Bitmap.SetSize(BitmapWidthEdit.Value, BitmapHeightEdit.Value);
  Target := Point(TargetXEdit.Value, TargetYEdit.Value);

  Bitmap.Canvas.Brush.Color := clWhite;
  Bitmap.Canvas.FillRect(BitmapRect);
  Bitmap.Canvas.TextOut(0, 0, 'Test XXX');

  InflateRect(BitmapRect, -4, -4);

  AlphaBlendImpl.AlphaBlend(0, Bitmap.Canvas.Handle, BitmapRect, Point(0,0), bmConstantAlphaAndColor, 128,
    ColorToRGB(BlendColorBox.Selected));

  BitBlt(Box.Canvas.Handle, Target.x, Target.y, BitmapWidthEdit.Value, BitmapHeightEdit.Value, Bitmap.Canvas.Handle, 0, 0, SRCCOPY);
end;

procedure TMainForm.FillRectPaintBoxPaint(Sender: TObject);
var
  ControlRect, BitmapRect: TRect;
  Target: TPoint;
  Box: TPaintBox absolute Sender;
  Bitmap: TBitmap;
begin
  ControlRect := Box.ClientRect;
  Box.Canvas.Brush.Color := clYellow;
  Box.Canvas.FillRect(ControlRect);

  BitmapRect := Rect(0, 0, BitmapWidthEdit.Value, BitmapHeightEdit.Value);
  Bitmap := TBitmap.Create;
  Bitmap.PixelFormat := pf32bit;
  Bitmap.SetSize(BitmapWidthEdit.Value, BitmapHeightEdit.Value);
  Target := Point(TargetXEdit.Value, TargetYEdit.Value);

  Bitmap.Canvas.Brush.Color := clWhite;
  Bitmap.Canvas.FillRect(BitmapRect);
  Bitmap.Canvas.TextOut(0, 0, 'Test XXX');

  InflateRect(BitmapRect, -4, -4);

  Bitmap.Canvas.Brush.Color := BlendColorBox.Selected;
  Bitmap.Canvas.FillRect(BitmapRect);

  BitBlt(Box.Canvas.Handle, Target.x, Target.y, BitmapWidthEdit.Value, BitmapHeightEdit.Value, Bitmap.Canvas.Handle, 0, 0, SRCCOPY);
end;

end.

