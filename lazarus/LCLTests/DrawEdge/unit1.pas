unit Unit1; 

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls, ColorBox;

type

  { TDrawEdgeForm }

  TDrawEdgeForm = class(TForm)
    ColorBox1: TColorBox;
    DrawEdgePaintBox: TPaintBox;
    DrawEdgeNoMiddlePaintBox: TPaintBox;
    DrawEdgeCustomPaintBox: TPaintBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    procedure ColorBox1Select(Sender: TObject);
    procedure DrawEdgeNoMiddlePaintBoxPaint(Sender: TObject);
    procedure DrawEdgeCustomPaintBoxPaint(Sender: TObject);
    procedure DrawEdgePaintBoxPaint(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  DrawEdgeForm: TDrawEdgeForm;

implementation

uses
  LCLIntf, LCLType;


{ TDrawEdgeForm }

procedure TDrawEdgeForm.DrawEdgePaintBoxPaint(Sender: TObject);
var
  R: TRect;
begin
  DrawEdgePaintBox.Canvas.Brush.Color := ColorBox1.Selected;
  R := Rect (10, 10, 110, 110);
  DrawEdgePaintBox.Canvas.FillRect(R.Left, R.Top, R.Right, R.Bottom);
  InflateRect(R, -1, -1);
  DrawEdge(DrawEdgePaintBox.Canvas.Handle, R, BDR_RAISEDINNER or BDR_RAISEDOUTER, BF_LEFT or BF_TOP or BF_BOTTOM or BF_RIGHT or BF_MIDDLE or BF_SOFT or BF_ADJUST);
end;

procedure TDrawEdgeForm.DrawEdgeNoMiddlePaintBoxPaint(Sender: TObject);
var
  R: TRect;
begin
  DrawEdgeNoMiddlePaintBox.Canvas.Brush.Color := ColorBox1.Selected;
  R := Rect (10, 10, 110, 110);
  DrawEdgeNoMiddlePaintBox.Canvas.FillRect(R.Left, R.Top, R.Right, R.Bottom);
  InflateRect(R, -1, -1);
  DrawEdge(DrawEdgeNoMiddlePaintBox.Canvas.Handle, R, BDR_RAISEDINNER or BDR_RAISEDOUTER, BF_LEFT or BF_TOP or BF_BOTTOM or BF_RIGHT or BF_SOFT or BF_ADJUST);
end;

procedure TDrawEdgeForm.ColorBox1Select(Sender: TObject);
begin
  Invalidate;
end;

procedure TDrawEdgeForm.DrawEdgeCustomPaintBoxPaint(Sender: TObject);
begin
  //inner right/bottom
  DrawEdgeCustomPaintBox.Canvas.Pen.Color := clBtnShadow;
  DrawEdgeCustomPaintBox.Canvas.MoveTo(10, 109);
  DrawEdgeCustomPaintBox.Canvas.LineTo(109, 109);

  DrawEdgeCustomPaintBox.Canvas.MoveTo(109, 10);
  DrawEdgeCustomPaintBox.Canvas.LineTo(109, 110);

  //outer rigth/bottom
  DrawEdgeCustomPaintBox.Canvas.Pen.Color := cl3DDkShadow;
  DrawEdgeCustomPaintBox.Canvas.MoveTo(10, 110);
  DrawEdgeCustomPaintBox.Canvas.LineTo(110, 110);

  DrawEdgeCustomPaintBox.Canvas.MoveTo(110, 10);
  DrawEdgeCustomPaintBox.Canvas.LineTo(110, 111);

  //tl inner
  DrawEdgeCustomPaintBox.Canvas.Pen.Color := cl3DLight;
  DrawEdgeCustomPaintBox.Canvas.MoveTo(11, 11);
  DrawEdgeCustomPaintBox.Canvas.LineTo(110, 11);

  DrawEdgeCustomPaintBox.Canvas.MoveTo(11, 11);
  DrawEdgeCustomPaintBox.Canvas.LineTo(11, 110);

  //tl outer
  DrawEdgeCustomPaintBox.Canvas.Pen.Color := cl3DHiLight;// = clBtnHiLight, clBtnHighlight;
  DrawEdgeCustomPaintBox.Canvas.MoveTo(10, 10);
  DrawEdgeCustomPaintBox.Canvas.LineTo(10, 110);

  DrawEdgeCustomPaintBox.Canvas.MoveTo(10, 10);
  DrawEdgeCustomPaintBox.Canvas.LineTo(110, 10);

  // sunken line
  DrawEdgeCustomPaintBox.Canvas.Pen.Color := clBtnHiLight;
  DrawEdgeCustomPaintBox.Canvas.MoveTo(122, 10);
  DrawEdgeCustomPaintBox.Canvas.LineTo(122, 110);


  DrawEdgeCustomPaintBox.Canvas.Pen.Color := cl3DShadow;
  DrawEdgeCustomPaintBox.Canvas.MoveTo(123, 10);
  DrawEdgeCustomPaintBox.Canvas.LineTo(123, 110);
end;

initialization
  {$I unit1.lrs}

end.

