unit unit1;

{$mode objfpc}{$H+}

interface

uses
  LCLType, Types, LclIntf, Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    PaintBox1: TPaintBox;
    PaintBox2: TPaintBox;
    PaintBox3: TPaintBox;
    procedure PaintBox1Paint (Sender: TObject );
    procedure PaintBox2Paint(Sender: TObject);
    procedure PaintBox3Paint(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  Form1: TForm1; 

implementation

function GenericExcludeClipRect(dc: hdc;
  Left, Top, Right, Bottom : Integer) : Integer;
var
  RRGN : hRGN;
begin
  If DCClipRegionValid(DC) then begin
    //DebugLn('TWidgetSet.ExcludeClipRect A DC=',DbgS(DC),' Rect=',Left,',',Top,',',Right,',',Bottom);
    // create the rectangle region, that should be excluded
    RRGN := CreateRectRgn(Left,Top,Right,Bottom);
    Result := ExtSelectClipRGN(DC, RRGN, RGN_DIFF);
    //DebugLn('TWidgetSet.ExcludeClipRect B Result=',Result);
    DeleteObject(RRGN);
  end else
    Result:=ERROR;
end;

function GenericExcludeClipRect2(dc: hdc;
  Left, Top, Right, Bottom : Integer) : Integer;
var
  RRGN : HRGN;
  R : TRect;
begin
  // there seems to be a bug in TWidgetset ExcludeClipRect.
  // as it doesn't use LPtoDP() (as IntersectClipRect does).
  // Fixing the problem here.
  R := Types.Rect(Left, Top, Right, Bottom);
  LPtoDP(DC, R, 2);

  If DCClipRegionValid(DC) then begin
    //DebugLn('TWidgetSet.ExcludeClipRect A DC=',DbgS(DC),' Rect=',Left,',',Top,',',Right,',',Bottom);
    // create the rectangle region, that should be excluded
    RRGN := CreateRectRgn(R.Left,R.Top,R.Right,R.Bottom);
    Result := ExtSelectClipRGN(DC, RRGN, RGN_DIFF);
    //DebugLn('TWidgetSet.ExcludeClipRect B Result=',Result);
    DeleteObject(RRGN);
  end else
    Result:=ERROR;
end;

{ TForm1 }

procedure TForm1.PaintBox1Paint (Sender: TObject );
begin
  with PaintBox1 do
  begin
    Canvas.Brush.Color := clBlue;
    Canvas.FillRect(0,0,Width,Height);
    ExcludeClipRect(Canvas.Handle,10,10,Width - 10,Height div 2);
    Canvas.Brush.Color:=clYellow;
    Canvas.FillRect(0,0,Width,Height);
  end;
end;

procedure TForm1.PaintBox2Paint(Sender: TObject);
begin
  with PaintBox2 do
  begin
    Canvas.Brush.Color := clBlue;
    Canvas.FillRect(0,0,Width,Height);
    GenericExcludeClipRect(Canvas.Handle,10,10,Width - 10,Height div 2);
    Canvas.Brush.Color:=clYellow;
    Canvas.FillRect(0,0,Width,Height);
  end;
end;

procedure TForm1.PaintBox3Paint(Sender: TObject);
begin
  with PaintBox3 do
  begin
    Canvas.Brush.Color := clBlue;
    Canvas.FillRect(0,0,Width,Height);
    GenericExcludeClipRect2(Canvas.Handle,10,10,Width - 10,Height div 2);
    Canvas.Brush.Color:=clYellow;
    Canvas.FillRect(0,0,Width,Height);
  end;
end;

initialization
  {$I unit1.lrs}

end.

