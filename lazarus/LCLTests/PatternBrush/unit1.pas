unit Unit1; 

{$mode objfpc}{$H+}


interface

uses
  LCLIntf, LCLType, Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, Buttons,
  ExtCtrls, Menus, StdCtrls, ColorBox;

type

  { TMainForm }

  TMainForm = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    MonoSolidPaintBox: TPaintBox;
    TestMonoSolidButton: TButton;
    TextColorEdit: TColorBox;
    BackgroundColorEdit: TColorBox;
    TestMonoButton: TButton;
    TestTiledButton: TButton;
    MonoPaintBox: TPaintBox;
    MultiPaintBox: TPaintBox;
    PopupMenu1: TPopupMenu;
    SolidTextColorEdit: TColorBox;
    procedure MonochromeColorEditEditingDone(Sender: TObject);
    procedure MonoSolidPaintBoxPaint(Sender: TObject);
    procedure SolidTextColorEditSelect(Sender: TObject);
    procedure TestMonoButtonClick(Sender: TObject);
    procedure TestMonoSolidButtonClick(Sender: TObject);
    procedure TestTiledButtonClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MonoPaintBoxPaint(Sender: TObject);
    procedure MultiPaintBoxPaint(Sender: TObject);
  private
    { private declarations }
      FDottedBrush: HBrush;
      FSolidBrush: HBrush;
      FBitmapBrush: HBrush;
  public
    { public declarations }
  end; 

var
  MainForm: TMainForm;

implementation

{$R *.lfm}

{ TMainForm }


procedure TMainForm.TestMonoButtonClick(Sender: TObject);
const
  Bits: array [0..8] of Word = ($55, $AA, $55, $AA, $55, $AA, $55, $AA, $55);
var
  FBitmap: HBitmap;
begin
  if FDottedBrush = 0 then
  begin
    FBitmap := CreateBitmap(8, 8, 1, 1, @Bits);
    FDottedBrush := CreatePatternBrush(FBitmap);
    DeleteObject(FBitmap);
  end;
  MonoPaintBox.Invalidate;
end;

procedure TMainForm.TestMonoSolidButtonClick(Sender: TObject);
const
  Bits: array [0..7] of Word = (0, 0, 0, 0, 0, 0, 0, 0);
var
  FBitmap: HBitmap;
begin
  if FSolidBrush = 0 then
  begin
    FBitmap := CreateBitmap(8, 8, 1, 1, @Bits);
    FSolidBrush := CreatePatternBrush(FBitmap);
    DeleteObject(FBitmap);
  end;
  MonoSolidPaintBox.Invalidate;
end;

procedure TMainForm.MonochromeColorEditEditingDone(Sender: TObject);
begin
  MonoPaintBox.Invalidate;
end;

procedure TMainForm.MonoSolidPaintBoxPaint(Sender: TObject);
begin
  if FSolidBrush <> 0 then
  begin
    MonoSolidPaintBox.Canvas.Font.Color := SolidTextColorEdit.Selected;
    FillRect(MonoSolidPaintBox.Canvas.Handle, Rect(0,0,MonoSolidPaintBox.Width,MonoSolidPaintBox.Height), FSolidBrush);
  end;
end;

procedure TMainForm.SolidTextColorEditSelect(Sender: TObject);
begin
  MonoSolidPaintBox.Invalidate;
end;

procedure TMainForm.TestTiledButtonClick(Sender: TObject);
var
  FBitmap: TBitmap;
begin
  if FBitmapBrush = 0 then
  begin
    FBitmap:= TBitmap.Create;
    with FBitmap do
    begin
      Width:=8;
      Height:=8;

      Canvas.Brush.Color:=clRed;
      Canvas.FillRect(0,0,4,4);
      Canvas.Brush.Color:=clWhite;
      Canvas.FillRect(4,0,8,4);
      Canvas.Brush.Color:=clBlue;
      Canvas.FillRect(0,4,4,8);
      Canvas.Brush.Color:=clYellow;
      Canvas.FillRect(4,4,8,8);

      FBitmapBrush := CreatePatternBrush(Handle);

      Destroy;
    end;
  end;
  MultiPaintBox.Invalidate;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  if FBitmapBrush <> 0 then
    DeleteObject(FBitmapBrush);
  if FDottedBrush <> 0 then
    DeleteObject(FDottedBrush);
  if FSolidBrush <> 0 then
    DeleteObject(FSolidBrush);
end;

procedure TMainForm.MonoPaintBoxPaint(Sender: TObject);
begin
  if FDottedBrush <> 0 then
  begin
    MonoPaintBox.Canvas.Font.Color := TextColorEdit.Selected;
    MonoPaintBox.Canvas.Brush.Color := BackgroundColorEdit.Selected;
    FillRect(MonoPaintBox.Canvas.Handle, Rect(0,0,MonoPaintBox.Width,MonoPaintBox.Height), FDottedBrush);
  end;
end;

procedure TMainForm.MultiPaintBoxPaint(Sender: TObject);
begin
  if FBitmapBrush <> 0 then
    FillRect(MultiPaintBox.Canvas.Handle, Rect(0,0,MultiPaintBox.Width,MultiPaintBox.Height), FBitmapBrush);
end;




end.

