unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, DBGrids,
  StdCtrls, ExtCtrls, ZConnection, ZDataset, mssqlconn, sqldb, db;

type

  { TMainForm }

  TMainForm = class(TForm)
    ConnectButton: TButton;
    DatabaseEdit: TLabeledEdit;
    HostnameEdit: TLabeledEdit;
    UserEdit: TLabeledEdit;
    PasswordEdit: TLabeledEdit;
    SQLEdit: TEdit;
    ExecuteButton: TButton;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    ZConnection1: TZConnection;
    ZQuery1: TZQuery;
    procedure ConnectButtonClick(Sender: TObject);
    procedure ExecuteButtonClick(Sender: TObject);
  private

  public

  end;

var
  MainForm: TMainForm;

implementation

{$R *.lfm}

{ TMainForm }

procedure TMainForm.ConnectButtonClick(Sender: TObject);
begin
  ZConnection1.Database := DatabaseEdit.Text;
  ZConnection1.HostName := HostnameEdit.Text;
  ZConnection1.User := UserEdit.Text;
  ZConnection1.Password := PasswordEdit.Text;
  ZConnection1.Connect;
end;

procedure TMainForm.ExecuteButtonClick(Sender: TObject);
begin
  ZQuery1.Close;
  ZQuery1.SQL.Text := SQLEdit.Text;
  ZQuery1.Open;
end;

end.

