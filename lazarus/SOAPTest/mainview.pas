unit MainView;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  XMLPropStorage;

type

  { TMainForm }

  TMainForm = class(TForm)
    EngineGroupBox: TRadioGroup;
    SendButton: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ResponseCodeLabel: TLabel;
    ResponseBodyMemo: TMemo;
    URLEdit: TLabeledEdit;
    RequestBodyMemo: TMemo;
    HeadersBodyMemo: TMemo;
    XMLPropStorage1: TXMLPropStorage;
    procedure SendButtonClick(Sender: TObject);
  private
    FLastError: String;
    function CURLPost(const URL: String; RequestBody, Headers, ResponseBody: TStrings; out
      Code: Integer): Boolean;
    function SoapPost(const URL: String; RequestBody, Headers, ResponseBody: TStrings;
      out Code: Integer): Boolean;
    function SynapsePost(const URL: String; RequestBody, Headers, ResponseBody: TStrings; out
      Code: Integer): Boolean;
  public

  end;

var
  MainForm: TMainForm;

implementation

uses
  httpsend, LCLProc, UTF8Process, process;

function TMainForm.SynapsePost(const URL: String; RequestBody, Headers, ResponseBody: TStrings;
  out Code: Integer): Boolean;
var
  Http: THTTPSend;
begin
  Http := THTTPSend.Create;
  try
    Http.Headers.Assign(Headers);
    RequestBody.SaveToStream(Http.Document);
    Http.Document.Position := 0;
    Http.MimeType := 'text/xml';
    Result := Http.HTTPMethod('POST', URL);
    if Result then
    begin
      ResponseBody.LoadFromStream(Http.Document);
      Code := Http.ResultCode;
    end
    else
      FLastError := Http.Sock.LastErrorDesc;
  finally
    Http.Destroy;
  end;
end;

function TMainForm.CURLPost(const URL: String; RequestBody, Headers, ResponseBody: TStrings;
  out Code: Integer): Boolean;
var
  RequestFileName, ResponseFileName, Params, ResponseCode: String;
  Process: TProcessUTF8;
begin
  ResponseFileName := GetTempFileName('', 'curl-response');
  RequestFileName := GetTempFileName('', 'curl-request');
  DebugLn('Request File: %s / Response File: %s', [RequestFileName, ResponseFileName]);

  //todo: handle headers

  RequestBody.SaveToFile(RequestFileName);
  Params := Format('-X POST -d @%s %s --header "Content-Type:text/xml" --output %s -w %%{http_code} -s', [RequestFileName, URL, ResponseFileName]);
  DebugLn('CURL Params ', Params);

  Process := TProcessUTF8.Create(nil);
  try
    Process.Executable := 'curl.exe';
    Process.Parameters.Text := Params;
    Process.Options := [poWaitOnExit, poUsePipes];
    Process.Execute;

    SetLength(ResponseCode, Process.Output.NumBytesAvailable);
    Process.Output.Read(ResponseCode[1], Process.Output.NumBytesAvailable);
    Code := StrToIntDef(ResponseCode, 0);
    Result := Process.ExitStatus = 0;
  finally
    Process.Destroy;
  end;

  if Result and FileExists(ResponseFileName) then
    ResponseBody.LoadFromFile(ResponseFileName);
  //DeleteFile(ResponseFileName);
  //DeleteFile(RequestFileName);
end;

{$R *.lfm}

{ TMainForm }

procedure TMainForm.SendButtonClick(Sender: TObject);
var
  Code: Integer;
begin
  if not SoapPost(URLEdit.Text, RequestBodyMemo.Lines, HeadersBodyMemo.Lines,
    ResponseBodyMemo.Lines, Code) then
  begin
    ShowMessage('Error posting: ' + FLastError);
  end
  else
  begin
    ResponseCodeLabel.Caption := Format('Code: %d', [Code]);
  end;
end;

function TMainForm.SoapPost(const URL: String; RequestBody, Headers, ResponseBody: TStrings; out
  Code: Integer): Boolean;
begin
  FLastError := '';
  case EngineGroupBox.ItemIndex of
    0: Result := SynapsePost(URL, RequestBody, Headers, ResponseBody, Code);
    1: Result := CURLPost(URL, RequestBody, Headers, ResponseBody, Code);
  end;
end;

end.

