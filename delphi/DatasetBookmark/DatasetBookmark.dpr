program DatasetBookmark;

{$APPTYPE CONSOLE}

uses
  SysUtils, StrUtils,
  Db,
  DBClient;

const
  StateNames: array[TDataSetState] of String = (
   'dsInactive', 'dsBrowse', 'dsEdit', 'dsInsert', 'dsSetKey',
    'dsCalcFields', 'dsFilter', 'dsNewValue', 'dsOldValue', 'dsCurValue', 'dsBlockRead',
    'dsInternalCalc', 'dsOpening'
  );

var
  Dataset: TClientDataSet;

procedure DumpDatasetState(Dataset: TDataset; NewLine: Boolean = False);
var
  S: String;
begin
  S := StateNames[Dataset.State];
  if Dataset.Active then
    S := S + ' RecNo: ' + IntToStr(Dataset.RecNo);
  Write(S + ' ');
  if NewLine then
    WriteLn;
end;

procedure DumpBookmark(const Name: String; Bookmark: TBookmark);
begin
  Write(Name);
  if Bookmark = nil then
    WriteLn(' - nil')
  else
  begin
    WriteLn(' (' + IfThen(Dataset.BookmarkValid(Bookmark), 'Valid', 'Invalid') + ') - ',
      IntToHex(Integer(Bookmark), 8), ' -> ', IntToHex(Integer(Bookmark^), 8));
  end;
end;

procedure DumpCompare(const Name: String; Bookmark1, Bookmark2: TBookmark);
begin
  WriteLn('CompareBookmarks - ', Name, ' ', Dataset.CompareBookmarks(Bookmark1, Bookmark2));
end;

var
  F: TField;
  ClosedBookmark, EmptyBookmark, FirstBookmark: TBookmark;
  AppendBookmark, PostAppendBookmark, EditBookmark, PostEditBookmark: TBookmark;
begin
  Dataset := TClientDataSet.Create(nil);

  F := TIntegerField.Create(Dataset);
  F.FieldName := 'Integer';
  F.DataSet := Dataset;
  F.FieldKind := fkData;

  Dataset.CreateDataSet;
  DumpDatasetState(Dataset);  
  EmptyBookmark := Dataset.GetBookmark;
  DumpBookmark('EmptyBookmark', EmptyBookmark);

  Dataset.Close;
  DumpDatasetState(Dataset);
  ClosedBookmark := Dataset.GetBookmark;
  DumpBookmark('ClosedBookmark', ClosedBookmark);

  Dataset.Open;
  Dataset.InsertRecord([1]);
  Dataset.InsertRecord([2]);
  Dataset.InsertRecord([3]);

  Dataset.First;    
  DumpDatasetState(Dataset);
  FirstBookmark := Dataset.GetBookmark;
  DumpBookmark('FirstBookmark', FirstBookmark);

  Dataset.Append;
  DumpDatasetState(Dataset);
  AppendBookmark := Dataset.GetBookmark;
  DumpBookmark('AppendBookmark', AppendBookmark);
  Dataset.Post;
  DumpDatasetState(Dataset);
  PostAppendBookmark := Dataset.GetBookmark;
  DumpBookmark('PostAppendBookmark', PostAppendBookmark);


  Dataset.Edit;
  DumpDatasetState(Dataset);
  EditBookmark := Dataset.GetBookmark;
  DumpBookmark('EditBookmark', EditBookmark);
  Dataset.Post;
  DumpDatasetState(Dataset);
  PostEditBookmark := Dataset.GetBookmark;
  DumpBookmark('PostEditBookmark', PostEditBookmark);

  Dataset.First;
  DumpDatasetState(Dataset, True);
  WriteLn('Goto EditBookmark');
  Dataset.GotoBookmark(EditBookmark);
  DumpDatasetState(Dataset, True);

  Dataset.First;
  DumpDatasetState(Dataset, True);
  WriteLn('Goto PostEditBookmark');
  Dataset.GotoBookmark(PostEditBookmark);
  DumpDatasetState(Dataset, True);

  DumpCompare('nil, nil', nil, nil);
  DumpCompare('nil, First', nil, FirstBookmark);
  DumpCompare('First, nil', FirstBookmark, nil);
  DumpCompare('First, PostEdit', FirstBookmark, PostEditBookmark);
  DumpCompare('Edit, PostEdit', EditBookmark, PostEditBookmark);  

  Dataset.Destroy;
end.
