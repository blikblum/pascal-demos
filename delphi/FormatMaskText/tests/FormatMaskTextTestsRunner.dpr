program FormatMaskTextTestsRunner;

uses
  Forms,
  TestFramework,
  GuiTestRunner,
  FormatMaskTextTests in 'FormatMaskTextTests.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Run;
  GUITestRunner.RunRegisteredTests;
end.
