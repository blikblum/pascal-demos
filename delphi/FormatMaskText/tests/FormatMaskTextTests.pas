unit FormatMaskTextTests;

interface

uses
  {$ifdef FPC}
  fpcunit, testregistry
  {$else}
  TestFramework
  {$endif}
  ;

type
  TFormatMaskTextTests = class(TTestCase)
  published
    procedure EmptyMask;
    procedure ArbitraryCharacters;
    procedure AlphabeticCharacters;    
    procedure AlphanumericCharacters;
    procedure NumericCharacters;
    procedure MatchLiterals;
    procedure InsertLiterals;
    procedure OptionalBlanksPosition;
    procedure ScapedCharacters;
  end;


implementation

uses
  {$ifdef TestLCLFormatMaskText}
  MaskEdit
  {$else}
  MaskUtils
  {$endif};

{ TFormatMaskTextTests }

procedure TFormatMaskTextTests.AlphabeticCharacters;
var
  Res: String;
begin
  Res := FormatMaskText('ll', 'test');
  CheckEquals('te', Res);

  Res := FormatMaskText('ll', 't');
  CheckEquals('t ', Res);

  Res := FormatMaskText('ll', '012345');
  CheckEquals('01', Res);

  Res := FormatMaskText('ll', '0');
  CheckEquals('0 ', Res);

  Res := FormatMaskText('ll', '###');
  CheckEquals('##', Res);

  Res := FormatMaskText('ll', '#');
  CheckEquals('# ', Res);

  Res := FormatMaskText('LL', 'test');
  CheckEquals('te', Res);

  Res := FormatMaskText('LL', 't');
  CheckEquals('t ', Res);

  Res := FormatMaskText('LL', '012345');
  CheckEquals('01', Res);

  Res := FormatMaskText('LL', '0');
  CheckEquals('0 ', Res);

  Res := FormatMaskText('LL', '###');
  CheckEquals('##', Res);

  Res := FormatMaskText('LL', '#');
  CheckEquals('# ', Res);
end;


procedure TFormatMaskTextTests.AlphanumericCharacters;
var
  Res: String;
begin
  Res := FormatMaskText('aa', 'test');
  CheckEquals('te', Res);

  Res := FormatMaskText('aa', 't');
  CheckEquals('t ', Res);

  Res := FormatMaskText('aa', '012345');
  CheckEquals('01', Res);

  Res := FormatMaskText('aa', '0');
  CheckEquals('0 ', Res);

  Res := FormatMaskText('aa', '###');
  CheckEquals('##', Res);

  Res := FormatMaskText('aa', '#');
  CheckEquals('# ', Res);

  Res := FormatMaskText('AA', 'test');
  CheckEquals('te', Res);

  Res := FormatMaskText('AA', 't');
  CheckEquals('t ', Res);

  Res := FormatMaskText('AA', '012345');
  CheckEquals('01', Res);

  Res := FormatMaskText('AA', '0');
  CheckEquals('0 ', Res);

  Res := FormatMaskText('AA', '###');
  CheckEquals('##', Res);

  Res := FormatMaskText('AA', '#');
  CheckEquals('# ', Res);
end;

procedure TFormatMaskTextTests.ArbitraryCharacters;
var
  Res: String;
begin
  Res := FormatMaskText('cc', 'test');
  CheckEquals('te', Res);

  Res := FormatMaskText('cc', 't');
  CheckEquals('t ', Res);

  Res := FormatMaskText('cc', '012345');
  CheckEquals('01', Res);

  Res := FormatMaskText('cc', '0');
  CheckEquals('0 ', Res);

  Res := FormatMaskText('cc', '###');
  CheckEquals('##', Res);

  Res := FormatMaskText('cc', '#');
  CheckEquals('# ', Res);
  //??
  Res := FormatMaskText('CC', 'test');
  CheckEquals('te', Res);

  Res := FormatMaskText('CC', 't');
  CheckEquals('t ', Res);

  Res := FormatMaskText('CC', '012345');
  CheckEquals('01', Res);

  Res := FormatMaskText('CC', '0');
  CheckEquals('0 ', Res);

  Res := FormatMaskText('CC', '###');
  CheckEquals('##', Res);

  Res := FormatMaskText('CC', '#');
  CheckEquals('# ', Res);
end;

procedure TFormatMaskTextTests.EmptyMask;
var
  Res: String;
begin
  Res := FormatMaskText('', 'test');
  CheckEquals('', Res);
end;

procedure TFormatMaskTextTests.InsertLiterals;
var
  Res: String;
begin
  Res := FormatMaskText('(cc;0', 'test');
  CheckEquals('(test', Res);

  Res := FormatMaskText('c(c;0', 'test');
  CheckEquals('t(est', Res);

  Res := FormatMaskText('cc(;0', 'test');
  CheckEquals('te(st', Res);
end;

procedure TFormatMaskTextTests.MatchLiterals;
var
  Res: String;
begin
  Res := FormatMaskText('(cc', 'test');
  CheckEquals('(  ', Res);

  Res := FormatMaskText('c(c', 'test');
  CheckEquals('t( ', Res);

  Res := FormatMaskText('cc(', 'test');
  CheckEquals('te(', Res);

  Res := FormatMaskText('(cc;1', 'test');
  CheckEquals('(  ', Res);

  Res := FormatMaskText('c(c;1', 'test');
  CheckEquals('t( ', Res);

  Res := FormatMaskText('cc(;1', 'test');
  CheckEquals('te(', Res);
end;

procedure TFormatMaskTextTests.NumericCharacters;
var
  Res: String;
begin
  Res := FormatMaskText('99', 'test');
  CheckEquals('te', Res);

  Res := FormatMaskText('99', 't');
  CheckEquals('t ', Res);

  Res := FormatMaskText('99', '012345');
  CheckEquals('01', Res);

  Res := FormatMaskText('99', '0');
  CheckEquals('0 ', Res);

  Res := FormatMaskText('99', '###');
  CheckEquals('##', Res);

  Res := FormatMaskText('99', '#');
  CheckEquals('# ', Res);

  Res := FormatMaskText('00', 'test');
  CheckEquals('te', Res); //??

  Res := FormatMaskText('00', 't');
  CheckEquals('t ', Res);

  Res := FormatMaskText('00', '012345');
  CheckEquals('01', Res);

  Res := FormatMaskText('00', '0');
  CheckEquals('0 ', Res);

  Res := FormatMaskText('00', '###');
  CheckEquals('##', Res);

  Res := FormatMaskText('00', '#');
  CheckEquals('# ', Res);
end;

procedure TFormatMaskTextTests.OptionalBlanksPosition;
var
  Res: String;
begin
  Res := FormatMaskText('cc', 'x');
  CheckEquals('x ', Res);

  Res := FormatMaskText('!cc', 'x');
  CheckEquals(' x', Res);
end;

const ControlChars : array[0..10] of string = ('l', 'L', 'a', 'A', '0', '9', 'c', 'C', '#', ';', '_');

procedure TFormatMaskTextTests.ScapedCharacters;
var
  Res, ControlChar: String;
  i: Integer;
begin
  for i := Low(ControlChars) to High(ControlChars) do
  begin
    ControlChar := ControlChars[i];
    Res := FormatMaskText('\' + ControlChar + 'cc', 'test');
    CheckEquals(ControlChar + '  ', Res);
    Res := FormatMaskText('\' + ControlChar + 'cc;0', 'test');
    CheckEquals(ControlChar + 'test', Res);
  end;
  //! char - special handling
  Res := FormatMaskText('\!cc', 'test');
  CheckEquals('!  ', Res);
  Res := FormatMaskText('\!cc;0', 'test');
  CheckEquals('te!st', Res); //??

  //scaped literal
  Res := FormatMaskText('\(cc', 'test');
  CheckEquals('(  ', Res);
  Res := FormatMaskText('\(cc;0', 'test');
  CheckEquals('(test', Res);
end;

initialization
  {$ifdef FPC}
  RegisterTest(TFormatMaskTextTests);
  {$else}
  TestFramework.RegisterTest('FormatMaskText', TFormatMaskTextTests.Suite);
  {$endif}

end.
