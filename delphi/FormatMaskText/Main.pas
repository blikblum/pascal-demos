unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    TextEdit: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    MaskEdit: TEdit;
    FormatButton: TButton;
    FormattedTextLabel: TLabel;
    procedure FormatButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  MaskUtils;

{$R *.dfm}

procedure TForm1.FormatButtonClick(Sender: TObject);
begin
  FormattedTextLabel.Caption := FormatMaskText(MaskEdit.Text, TextEdit.Text);
end;

end.
