object Form1: TForm1
  Left = 605
  Top = 306
  Width = 290
  Height = 159
  Caption = 'FormatMaskText Demo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 21
    Height = 13
    Caption = 'Text'
  end
  object Label2: TLabel
    Left = 142
    Top = 8
    Width = 26
    Height = 13
    Caption = 'Mask'
  end
  object FormattedTextLabel: TLabel
    Left = 8
    Top = 96
    Width = 94
    Height = 13
    Caption = 'FormattedTextLabel'
  end
  object TextEdit: TEdit
    Left = 8
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'TextEdit'
  end
  object MaskEdit: TEdit
    Left = 142
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 1
    Text = 'MaskEdit'
  end
  object FormatButton: TButton
    Left = 8
    Top = 56
    Width = 257
    Height = 25
    Caption = 'FormatMaskText'
    TabOrder = 2
    OnClick = FormatButtonClick
  end
end
