program DatasetBindFieldsException;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  db,
  dbf in 'tdbf\dbf.pas';

var
  ds: TDbf;
  F: TField;

begin
  DeleteFile('data.dbf');
  ds := TDbf.Create(nil);
  ds.FieldDefs.Add('Id', ftInteger);
  ds.FilePathFull := ExtractFilePath(ParamStr(0));
  ds.TableName := 'data.db';
  ds.CreateTable;
  ds.Open;
  ds.Close;
  F := TStringField.Create(ds);
  F.FieldName:='DOES_NOT_EXIST';
  F.DataSet:=ds;
  F.Size:=50;
  ds.Open;
end.
