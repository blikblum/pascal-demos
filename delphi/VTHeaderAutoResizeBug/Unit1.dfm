object Form1: TForm1
  Left = 445
  Top = 208
  Width = 581
  Height = 342
  Caption = 'VT Header Auto Resize Bug'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    565
    303)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 11
    Top = 12
    Width = 83
    Height = 13
    Caption = 'Auto Size Column'
  end
  object MainPanel: TPanel
    Left = 7
    Top = 39
    Width = 553
    Height = 257
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    TabOrder = 0
  end
  object AutoSizeIndexEdit: TSpinEdit
    Left = 98
    Top = 8
    Width = 45
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 1
    Value = 0
  end
  object UpdateButton: TButton
    Left = 187
    Top = 6
    Width = 75
    Height = 25
    Caption = 'Update'
    TabOrder = 2
    OnClick = UpdateButtonClick
  end
end
