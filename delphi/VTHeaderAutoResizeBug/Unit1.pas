unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, VirtualTrees, StdCtrls, Spin, ExtCtrls;

type
  TForm1 = class(TForm)
    MainPanel: TPanel;
    AutoSizeIndexEdit: TSpinEdit;
    Label1: TLabel;
    UpdateButton: TButton;
    procedure FormCreate(Sender: TObject);
    procedure UpdateButtonClick(Sender: TObject);
  private
    FTree: TVirtualStringTree;
    procedure VTGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
       Column: TColumnIndex; TextType: TVSTTextType; var CellText: WideString);
    procedure CreateTree(AutoSizeColumn: Integer);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

const
  ColumnCount = 6;
  AutoSizeColumn = 2;
  RowCount = 10;

procedure TForm1.FormCreate(Sender: TObject);
begin
  CreateTree(AutoSizeColumn);
  AutoSizeIndexEdit.Value := AutoSizeColumn;
  AutoSizeIndexEdit.MaxValue := ColumnCount - 1;
end;

procedure TForm1.VTGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; TextType: TVSTTextType; var CellText: WideString);
begin
  CellText := Format('Col %d Row %d', [Column, Node^.Index]);
end;

procedure TForm1.CreateTree(AutoSizeColumn: Integer);
var
  i: Integer;
begin
  FTree.Free;
  FTree := TVirtualStringTree.Create(Self);
  with FTree do
  begin
    BeginUpdate;

    with TreeOptions do
    begin
      AnimationOptions := [];
      AutoOptions      := [];
      MiscOptions      := [toFullRepaintOnResize, toGridExtensions, toWheelPanning];
      PaintOptions     := [toShowButtons, toShowVertGridLines, toFullVertGridLines,
                           toShowHorzGridLines,
                           toThemeAware, toUseBlendedImages];
      SelectionOptions := [toExtendedFocus, toFullRowSelect];
      StringOptions    := [];
    end;

    with Header do
    begin
      Options := [hoAutoResize, hoColumnResize, hoDblClickResize, hoVisible,
                  hoFullRepaintOnResize];

      for i := 0 to ColumnCount - 1 do
      begin
        with Columns.Add do
        begin
          Text := 'Col ' + IntToStr(i);
          if i = AutoSizeColumn then Text := Text + ' (autosize)';
          CheckBox   := true;
          CheckType  := ctNone;
          Options    := [coAllowClick, coEnabled, coParentBidiMode,
                         coParentColor, coResizable, coVisible,
                         coSmartResize, coAllowFocus];
        end;
      end;

      MainColumn := 0;
      AutoSizeIndex := AutoSizeColumn;
    end;

    Align := alClient;
    Parent := MainPanel;
    OnGetText := VTGetText;

    EndUpdate;

    RootNodeCount := RowCount;
  end;

end;

procedure TForm1.UpdateButtonClick(Sender: TObject);
begin
  CreateTree(AutoSizeIndexEdit.Value);
end;

end.
